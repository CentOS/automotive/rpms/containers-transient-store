Name:           containers-transient-store
Version:        0.1
Release:        1%{?dist}
Summary:        Configuration to run containers in transient mode

License:        GPL-2.0+

Source0:        var-lib-containers-storage.mount

BuildArch:      noarch
BuildRequires:  systemd-rpm-macros

%define debug_package %{nil}

%description
This is a set of systemd mount unit that makes the podman container
storage be transient. In other words, no containers are persisted
across reboots.

%prep
rm -rf %{name}-{%version}
mkdir %{name}-{%version}

%build
cd %{name}-{%version}

%install
cd %{name}-{%version}
mkdir -p %{buildroot}%{_unitdir}
install -m 0644 "%{SOURCE0}" "%{buildroot}%{_unitdir}/var-lib-containers-storage.mount"

%post
systemctl --no-reload enable var-lib-containers-storage.mount

%files
%{_unitdir}/var-lib-containers-storage.mount

%changelog
* Wed Oct 12 2022 Alexander Larsson <alexl@redhat.com> - 0.1-1
- Initial version
